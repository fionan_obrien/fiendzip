Description

I wanted to create a fully stand alone html file, that could

    encrypt its contents
    hold all the tools to decrypt itself
    reproduce itself
    entirely worked in local browser - (no server calls)
    extensible (simply adding more functions)
    be self contained and not require any extensions such as JQuery or any Encrpyt Library

 

Example : unlocked

 

FiendZip is an implementation of of Fiend Encryption algorithm that i created to conform to the above constraints.

A FiendZip.html carries the code required to take a file, convert it to Base64, Encode  and Decode using a Cipher table, 

The strength in the encryption comes from the Randomly generated Cipher table created on the instance of encryption.

 
How Fiend Encryption works:

 
Create Cipher Table

At the point of encryption a table is generated using the STRING values of "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=" in a randomised order.

Also a Random Set of modifier functions "MODIFIERS":{"ADD":0,"SUBTRACT":1,"FWD":2,"BCK":3,"ROTATE":4,"IGNORE":5} are associated with each Character entry

A modifier is a path through the cipher table and is explained in more detail later.

 

 Example table:
0 	1 	2 	3 	4 	5 	6 	7 	8 	9 	10 	11 	12 	13 	14 	15 	16 	17 	18 	19 	20 	21 	22 	23 	24 	25 	26 	27 	28 	29 	30 	31 	32 	33 	34 	35 	36 	37 	38 	39 	40 	41 	42 	43 	44 	45 	46 	47 	48 	49 	50 	51 	52 	53 	54 	55 	56 	57 	58 	59 	60 	61 	62 	63 	64
A 	B 	C 	D 	E 	F 	G 	H 	I 	J 	K 	L 	M 	N 	O 	P 	Q 	R 	S 	T 	U 	V 	W 	X 	Y 	Z 	a 	b 	c 	d 	e 	f 	g 	h 	i 	j 	k 	l 	m 	n 	o 	p 	q 	r 	s 	t 	u 	v 	w 	x 	y 	z 	0 	1 	2 	3 	4 	5 	6 	7 	8 	9 	+ 	/ 	=
W 	M 	P 	w 	b 	C 	1 	n 	r 	y 	6 	E 	= 	v 	F 	G 	7 	p 	m 	d 	x 	0 	N 	4 	u 	A 	t 	Z 	o 	U 	3 	B 	V 	i 	J 	Y 	z 	S 	8 	H 	2 	k 	L 	+ 	g 	a 	T 	f 	O 	I 	D 	9 	5 	e 	l 	Q 	c 	j 	K 	X 	/ 	s 	R 	h 	q
0 	2 	1 	5 	5 	1 	4 	5 	1 	3 	3 	3 	5 	4 	4 	4 	0 	5 	3 	3 	0 	4 	0 	0 	2 	1 	1 	2 	5 	2 	4 	5 	3 	4 	2 	4 	5 	1 	3 	1 	0 	3 	1 	0 	0 	2 	1 	3 	2 	3 	1 	5 	0 	5 	2 	2 	1 	0 	3 	2 	0 	4 	3 	4 	0

 

 

 

 

 

 
Take user Password

The password's characters are used several times within the program:

    The block size -
        This is the chunk of the code that the Modifier set will be applied to e.g if complexity = 22 the modifier set will be applied in chunks of 22 characters
    The choice of Modifier Functions
    As a variable within the ADD & SUBTRACT functions. 

Example : interface

 
Apply Modifiers Function

Using each block the appropriate modifiers are applied.

at this time there are 6 functions

    ADD function = adds the current character's ciphered index and the current password character's ciphered index and returns the cipher'd value.
    SUBTRACT function =  subtracts  the current character's ciphered index and the current password character's ciphered index and returns the cipher'd value.
    FORWARD function =  returns the Ciphered value of the Ciphered value of the current character
    BACKWARD function = returns the Alphabet value of the Ciphered Value of the current character
    ROTATE function  = swaps the current character with its opposite within the current block, and returns this characters cipher'd value
    IGNORE function - returns the Ciphered Value of the current character without any further modification.

 
Produce a html file for the user to download

After the cipher has been implemented on the given file, a new html page is downloaded to the client browser  complete with all the information of the encryption session but the password. 

This page is now encrypted and the user can close the original page -(nothing is stored on the server this is all done locally on the browser).

This page is transferable as an attached file and can be opened on another users browser.

The new page also has the ability to spawn new encrypted pages, and any modification of an encrypted pages source can allow for any extensibility a developer might desire for their own implementation, E.g add/remove functions, change implantation of how the password cascades through the encryption etc.

 

Example: downloaded

 

Please see below for a working implantation of FiendZip  for storing a small image only at this time.
